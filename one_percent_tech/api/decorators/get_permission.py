from collections.abc import Callable
from functools import wraps
from typing import Any, Literal, Self

from rest_framework.generics import GenericAPIView
from rest_framework.permissions import BasePermission

# Reference:
# https://chase-seibert.github.io/blog/2013/12/17/python-decorator-optional-parameter.html
# https://django-role-permissions.readthedocs.io/en/stable/

HTTPMethod = Literal["GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS"]


class GetPermission:
    def __init__(
        self: Self,
        method_permission: dict[HTTPMethod, list[type[BasePermission] | BasePermission]]
        | None = None,
    ) -> None:
        if method_permission is None:
            method_permission = {}
        self.method_permission = method_permission

    def __call__(self: Self, func: Callable[..., Any]) -> Callable[..., Any]:
        @wraps(func)
        def callable(obj: GenericAPIView, *args: Any, **kwargs: Any) -> Any:
            method = obj.request.method
            if method in self.method_permission:
                obj.permission_classes = self.method_permission[method]
            return func(obj, *args, **kwargs)

        return callable
