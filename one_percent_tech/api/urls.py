from django.urls import include, path
from drf_spectacular.views import SpectacularAPIView, SpectacularRedocView, SpectacularSwaggerView

from api.views import UserDataApiView

app_name = "api"

urlpatterns = [
    path("account/", include("dj_rest_auth.urls")),
    path("user-data/", UserDataApiView.as_view(), name="user-data"),
]

urlpatterns.extend(
    [
        path("schema/", SpectacularAPIView.as_view(), name="schema"),
        path(
            "swagger/",
            SpectacularSwaggerView.as_view(url_name="api:schema"),
            name="swagger",
        ),
        path(
            "redoc/",
            SpectacularRedocView.as_view(url_name="api:schema"),
            name="redoc",
        ),
    ]
)
