import logging
from typing import Any, Self

from django.contrib.auth import get_user_model
from django.http import HttpRequest

# from django.utils.decorators import method_decorator
from drf_spectacular.utils import extend_schema

# https://www.django-rest-framework.org/api-guide/status-codes/
from rest_framework import serializers, status
from rest_framework.generics import GenericAPIView

# from rest_framework.parsers import FormParser, JSONParser, MultiPartParser
# from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

# from dj_rest_auth.jwt_auth import JWTCookieAuthentication
from rest_framework_simplejwt.authentication import JWTAuthentication

from api.decorators.get_permission import GetPermission

# from drf_spectacular.utils import OpenApiResponse


logger = logging.getLogger(__name__)


class UserDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()  # 使用你定義的用戶模型
        fields = ["id", "username", "email", "password"]  # 根據你的需求選擇需要的字段


class UserDataApiView(GenericAPIView):
    authentication_classes = [JWTAuthentication]
    permission_classes = [IsAuthenticated]
    serializer_class = UserDataSerializer

    @GetPermission(method_permission={"GET": [IsAuthenticated], "POST": [IsAuthenticated]})
    def get_permissions(self: Self) -> Any:
        """
        Override this method to customize permissions for different methods.
        """
        return super().get_permissions()

    @extend_schema(
        tags=["System"],
        summary="取得使用者資訊",
        description="[JWT] [GET] /api/user-data/",
        responses={200: UserDataSerializer, 401: str},
    )
    def get(self: Self, request: HttpRequest) -> Response:
        user = get_user_model()
        current_user = user.objects.filter(id=request.user.id).first()
        logger.error("test error")
        if current_user:
            serializer = self.get_serializer(current_user)
            return Response(serializer.data)
        return Response({"detail": "User not found."}, status=status.HTTP_404_NOT_FOUND)

    @extend_schema(
        tags=["System"],
        summary="建立新使用者",
        description="[JWT] [POST] /api/user-data/",
        responses={200: UserDataSerializer, 401: str},
    )
    def post(self: Self, request: HttpRequest) -> Response:
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            # 在這裡處理經過驗證的數據，例如創建或更新用戶數據
            user = get_user_model()
            user.objects.create(**serializer.validated_data)
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
