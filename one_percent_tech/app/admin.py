from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from app.models import Profile

# 先卸載 User 的 admin site
admin.site.unregister(User)


# 重新註冊 User 的 admin site
class ProfileInline(admin.StackedInline):
    model = Profile
    verbose_name = "個人資料"
    verbose_name_plural = "個人資料2"


class UserInlineAdmin(UserAdmin):
    inlines = [
        ProfileInline,
    ]


admin.site.register(User, UserInlineAdmin)
