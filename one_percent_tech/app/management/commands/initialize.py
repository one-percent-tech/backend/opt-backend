import os

# Typing
from argparse import ArgumentParser
from getpass import getpass
from typing import Any, Self

from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

# from base.roles import Operator, Saas


class Command(BaseCommand):
    help = "Initialize procedures"

    def add_arguments(self: Self, parser: ArgumentParser) -> None:
        parser.add_argument("-f", "--force", action="store_true", help="force import")
        parser.add_argument("-d", "--demo", action="store_true", help="demo data")

    def handle(self: Self, **options: Any) -> None:
        if options["force"]:
            self.stdout.write("Clear registry...")

            # clear build
            self.stdout.write("Clear build...")

            self.stdout.write("Creating superuser...")
            self.create_superuser()

            self.stdout.write("Initialize done.")

        if options["demo"]:
            # self.stdout.write("Creating demo data...")
            # self.demo()
            pass

    def create_superuser(self: Self) -> None:
        username = os.environ.get("DJANGO_SUPERUSER_USERNAME") or input("Please enter username: ")
        email = os.environ.get("DJANGO_SUPERUSER_EMAIL") or input("Please enter email: ")
        password = os.environ.get("DJANGO_SUPERUSER_PASSWORD") or getpass("Please enter password: ")

        if not username:
            self.stdout.write("No given username, abort creating superuser...")
            return

        try:
            user = get_user_model()
            if username and user.objects.filter(username=username).exists():
                self.stdout.write("[Warning] User already exists", self.style.WARNING)
                return

            # admin
            user.objects.create_superuser(username, email, password)
        except Exception as e:
            self.stdout.write(f"[Error]: {e}", self.style.ERROR)
