from typing import Self

from django.contrib.auth.models import User
from django.db import models
from model_utils.models import TimeStampedModel

# Create your models here.


class Profile(TimeStampedModel):
    """
    使用者個人資料
    TimeStampedModel:
        created: 建立時間
        modified: 修改時間
    """

    class Gender(models.TextChoices):
        MALE = "male", "男"
        FEMALE = "female", "女"

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    gender = models.CharField("性別", max_length=16, choices=Gender.choices, blank=True)

    def __str__(self: Self) -> str:
        title: str = str(self.user.username) if self.user.username else ""
        return title
