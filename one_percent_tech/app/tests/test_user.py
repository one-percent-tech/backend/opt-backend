from typing import Self

from django.contrib.auth.models import User
from django.test import TestCase

from app.models import Profile


class ProfileTestCase(TestCase):
    def setUp(self: Self) -> None:
        lion = User.objects.create_user(username="lion", password="123456")
        cat = User.objects.create_user(username="cat", password="123456")
        # Profile
        lion_pro = Profile.objects.create(user=lion)
        cat_pro = Profile.objects.create(user=cat)

        lion_pro.gender = "male"
        lion_pro.save()
        cat_pro.gender = "female"
        cat_pro.save()

    def test_animals_gender(self: Self) -> None:
        male = Profile.objects.filter(user__profile__gender="male").first()
        female = Profile.objects.filter(user__profile__gender="female").first()
        self.assertEqual(male.user.username, "lion")
        self.assertEqual(female.user.username, "cat")
