# import subprocess and run the command
# use argparse to get the command from the user
import subprocess

from termcolor import colored

scripts = [
    {
        "task": "Init pre-commit",
        "commands": ["make", "init-pre-commit"],
    },
    {
        "task": "Init Makemigrations",
        "commands": ["make", "makemigrations"],
    },
    {
        "task": "Init Migration",
        "commands": ["make", "migrate"],
    },
    {
        "task": "Init Admin user",
        "commands": ["make", "init"],
    },
]


def main() -> None:
    # get the output of the command
    for script in scripts:
        try:
            print(colored(script["task"], "green", attrs=["bold"]))
            output = subprocess.run(script["commands"], capture_output=True, text=True)
            print(output.stdout)
        except Exception as e:
            print(colored(f"[Error]: {e}", "red", attrs=["bold"]))


if __name__ == "__main__":
    main()
