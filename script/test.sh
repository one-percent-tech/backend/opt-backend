#!/usr/bin/env sh
set -e

export PREFIX='python3 -m '
${PREFIX}pytest -n auto -vv
