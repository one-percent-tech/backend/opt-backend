# include .env
# export $(shell sed 's/=.*//' .env)

POETRY_PREFIX=poetry run
MANAGE_PATH=one_percent_tech/manage.py

.PHONY: help run format lint test test-tag test-path init init-pre-commit rm-migrations shell migrate makemigrations collectstatic erd test-cov test-report test-allure allure-serve


help:
	@echo "Usage: make [command]"
	@echo "Available commands:"
	@echo "  ----------------Common----------------"
	@echo "  init-pre-commit  - Install pre-commit and pre-push hooks"
	@echo "  format           - Format code using black and isort"
	@echo "  lint             - Lint code using flake8"
	@echo "  ----------------Django----------------"
	@echo "  run              - Run a command within the app environment"
	@echo "  shell            - Open a Django shell with SQL print"
	@echo "  migrate          - Apply database migrations"
	@echo "  makemigrations   - Create new database migrations"
	@echo "  collectstatic    - Collect static files"
	@echo "  init             - Initialize the project"
	@echo "  rm-migrations    - Remove migration files"
	@echo "  erd              - Generate ERD diagram"
	@echo "  ----------------Test------------------"
	@echo "  test             - Run tests with pytest"
	@echo "                     e.g. make test-tag t=tag_name"
	@echo "  test-tag         - Run tests filtered by tags with detailed output"
	@echo "  test-path        - Run tests on specific paths"
	@echo "                     e.g. make test-path p=one_percent_tech/tests/test_file.py"
	@echo "  test-cov         - Generate a coverage report"
	@echo "  test-report      - Display the coverage report"
	@echo "  test-allure      - Generate an allure report for tests"
	@echo "  allure-serve     - Serve the allure report locally"



# Common
init-pre-commit:
	$(POETRY_PREFIX) pre-commit install
	$(POETRY_PREFIX) pre-commit install -t pre-push

format:
	$(POETRY_PREFIX) ruff format

lint:
	$(POETRY_PREFIX) ruff check --fix
	$(POETRY_PREFIX) ruff check --select I --fix

# Django commands
run:
	$(POETRY_PREFIX) python $(MANAGE_PATH) runserver 0.0.0.0:3006

shell:
	$(POETRY_PREFIX) python $(MANAGE_PATH) shell_plus --print-sql

migrate:
	$(POETRY_PREFIX) python $(MANAGE_PATH) migrate

makemigrations:
	$(POETRY_PREFIX) python $(MANAGE_PATH) makemigrations

collectstatic:
	$(POETRY_PREFIX) python $(MANAGE_PATH) collectstatic

init:
	$(POETRY_PREFIX) python $(MANAGE_PATH) initialize -f

rm-migrations:
	find . -path '*/migrations/__init__.py' -exec truncate -s 0 {} + -o -path '*/migrations/*' -delete

# Schema 關係圖
erd:
	$(shell $(POETRY_PREFIX) python $(MANAGE_PATH) graph_models -a > ./doc/erd/erd.dot && $(POETRY_PREFIX) python $(MANAGE_PATH) graph_models --pydot -a -g -o ./doc/erd/erd.png)

# Test

test:
	$(POETRY_PREFIX) pytest -n auto -vv

test-tag:
	$(POETRY_PREFIX) pytest -s -v --allure-stories $(t)
# $(POETRY_PREFIX) pytest -s -v  --hypothesis-show-statistics --hypothesis-verbosity=verbose --allure-stories $(t)

test-path:
	$(POETRY_PREFIX) pytest $(p)

test-cov:
	$(POETRY_PREFIX) pytest --cov=ami --cov-report=html:htmlcov --cov-fail-under=60 -s -v -n auto

test-report:
	$(POETRY_PREFIX) coverage report

test-allure:
	$(POETRY_PREFIX) pytest -s -v -n auto --alluredir=allure-report

allure-serve:
	$(POETRY_PREFIX) allure serve allure-report -h 0.0.0.0 -p 5056


# Poetry
