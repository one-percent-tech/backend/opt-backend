# Allure2 + pytest 監測測試報告

## 安裝 allure2

```bash
curl -o allure-2.13.8.tgz -OLs https://repo.maven.apache.org/maven2/io/qameta/allure/allure-commandline/2.13.8/allure-commandline-2.13.8.tgz
sudo tar -zxvf allure-2.13.8.tgz -C /opt/
sudo ln -s /opt/allure-2.13.8/bin/allure /usr/bin/allure
allure --version
```

## 安裝 pipenv dev 環境

```bash
pipenv install --dev
```

## 安裝 allure-pytest

```bash
pip install allure-pytest
```

## 執行測試

```bash
make test-allure
```

## 運行 allure 服務

```bash
make allure-serve

>>>
Generating report to temp directory...
Report successfully generated to /tmp/427638109087994805/allure-report
Starting web server...
2023-09-03 00:29:57.073:INFO::main: Logging initialized @2088ms to org.eclipse.jetty.util.log.StdErrLog
Can not open browser because this capability is not supported on your platform. You can use the link below to open the report manually.
Server started at <http://0.0.0.0:5056/>. Press <Ctrl+C> to exit
```

打開 `http://0.0.0.0:5056/` 即可看到 allure2 的測試報告
