#!/bin/sh

# python one_percent_tech/manage.py createschema
python one_percent_tech/manage.py makemigrations
python one_percent_tech/manage.py migrate
python one_percent_tech/manage.py initialize -f
python one_percent_tech/manage.py runserver 0.0.0.0:3006
