# One%Tech Backend

- [One%Tech Backend](#onetech-backend)
  - [Description](#description)
  - [Contribution Guide](#contribution-guide)
    - [Setup Environment](#setup-environment)
    - [For Frontend Dev environment in docker](#for-frontend-dev-environment-in-docker)
  - [VScode guide](#vscode-guide)
    - [Debug](#debug)
    - [Tasks](#tasks)

---

## Description

`One%Tech Backend` is a project that utilizes `django 4.2` to build an API server.

## Contribution Guide

You need to install the following tools to setup the environment.

`Make`： Execute `Makefile` to setup everything

`Pyenv`： Control python version and install python library

`Python3.12.3`： Current python version of `3.12.3`

`Docker`: Containerize the project for deployment in the `dev` and `prod` environments

`Git`： Version control system

`Pre-commit`： Check code style before commit

`VScode(Optional)`： IDE for development, great for debugging and testing

### Setup Environment

- Copy `.env.example` to `.env` and change the environment variable

  ```sh
  cp .env.example .env
  ```

- Install `Python3.12.3` with `Pyenv` for python environment

  - Reference [Pipenv](https://www.notion.so/onepa-tech/Pyenv-8ae367a54adc498591ab6d1107ff0f2f)🍥

  - After installed `Pyenv`, you can install `Python3.12.3` and create a `virtual environment` by following command.

  ```sh
  pyenv install 3.12.3
  pyenv virtualenv 3.12.3 <your-virtualenv-name>
  ```

- Activate the virtual environment

  ```sh
  pyenv activate <your-virtualenv-name>
  ```

- Install `Poetry` package manager for `Python` project

  - Reference [Poetry official installation doc](https://python-poetry.org/docs/#installing-with-the-official-installer)
  - Reference [Python 套件管理器——Poetry 完全入門指南](https://blog.kyomind.tw/python-poetry/#pip-%E6%9B%BF%E4%BB%A3%E6%96%B9%E6%A1%88%E9%81%B8%E6%93%87%E2%80%94%E2%80%94Pipenv-vs-Poetry)
  - Reference [Poetry + Pyenv 教學：常用指令與注意事項](https://blog.kyomind.tw/poetry-pyenv-practical-tips/)
  - After installed `Poetry`, you can run the following command to install the project dependencies.

  ```sh
  poetry install
  ```

- Install `Docker` for containerize the project

  - Reference [Docker official installation doc](https://docs.docker.com/engine/install/)

- Enable `Postgresql` storage by docker compose, you can run the following command to start the `Postgresql` service at `5432` port.

  ```sh
  docker compose -f docker-compose-dev.yml up django-db -d
  ```

  > ⭐️ **NOTICE**
  >
  > If you are `Mac` or `Windows` user, you can install `Docker Desktop` directly.

- Insatll `make` command for executing `Makefile`

  ```sh
  # Ubuntu, Debian
  sudo apt-get install make

  # MacOS
  brew install make

  # Windows
  choco install make
  ```

- Looking for `Makefile` commands? Here are some useful command `make help`, show all command.

  ```sh
  make help

  Usage: make [command]
  Available commands:
  ----------------Common----------------
  init-pre-commit  - Install pre-commit and pre-push hooks
  format           - Format code using black and isort
  lint             - Lint code using flake8
  ----------------Django----------------
  ...
  ```

- Initialize the project by poetry script

  ```sh
  poetry run init
  ```

- Run server by following command, server will start at `http://localhost:3006`

  ```sh
  make run
  ```

### For Frontend Dev environment in docker

If you want to `develop the frontend within backend` you can use following command to start the backend server, then you can access the backend by `http://localhost:3006`.

```sh
docker compose -f docker-compose-dev.yml up -d
```

If you want to tear down the environment, you can use following command.

```sh
# keep the volume
docker compose -f docker-compose-dev.yml down

# remove the volume
docker compose -f docker-compose-dev.yml down -v
```

## VScode guide

### Debug

`Vscode` provides `debug tool` for `django` projects.

1. Install `vscode` extension `Python` and `Debugger for Chrome`.
2. Create `.vscode/launch.json` file, then copy the following content.

```json
{
  "version": "0.2.0",
  "configurations": [
    {
      "name": "Python: Django",
      "type": "python",
      "request": "launch",
      "program": "${workspaceFolder}/one_percent_tech/manage.py",
      "python": "${command:python.interpreterPath}",
      "args": ["runserver", "0.0.0.0:3006"],
      "django": true,
      "justMyCode": true
    }
  ]
}
```

### Tasks

This is good way to clean up all data and restart new environment when you develop locally.

- Create `settings.json` file in your `.vscode` folder. Then insert following context.
- Replace python path by you own.

```json
{
  "python.pythonPath": "path/to/your/python/bin/activate"
}
```

- Create `tasks.json` file in your `.vscode` folder. Then run `Init django` task, it will automatically run all tasks, you can refer to the following content.

```json
{
  "version": "2.0.0",
  "tasks": [
    {
      "label": "Remove migrations",
      "type": "shell",
      "command": "make",
      "args": ["rm-migrations"],
      "group": {
        "kind": "build",
        "isDefault": true
      }
    },
    {
      "label": "Create db schema",
      "dependsOn": ["Remove migrations"],
      "type": "shell",
      "command": "source `echo ${command:python.interpreterPath} | sed 's/python/activate/g'`; make create-schema",
      "group": {
        "kind": "build",
        "isDefault": true
      }
    }
  ]
}
```
